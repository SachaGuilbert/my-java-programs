public class UgeEt {
    public static void main(String[] args) {
        Opg5();
    }
    public static void Opg1(){
        System.out.println("Opgave 1");
        for(int i=1;i<11;i++){
            System.out.println(i*i);
        }
    }
    public static void Opg2(){
        System.out.println("Opgave 2");
        for(int i=0;i<5;i++){
            for(int j=1;j<11;j++){
                for(int k=0;k<10-j;k++){
                    System.out.print(10-j);
                }
            }
        System.out.println("");
        }
    }
    public static void Opg3(){
        System.out.println("Opgave 3");
        int prev2phi = 0;
        int prevphi = 1;
        int phi = 1;
        System.out.println(phi);
        for(int i=0;i<11;i++){
            phi = prevphi + prev2phi;
            prev2phi = prevphi;
            prevphi = phi;
            System.out.println(phi);
        }
    }
    public static void Opg4(){
        System.out.println("Opgave 4");
        for(int i=0;i<5;i++){
            for(int j=0;j<5-i;j++){
                System.out.print("-");
            }
            for(int j=0;j<i*2+1;j++){
                System.out.print(i*2+1);
            }
            for(int j=0;j<5-i;j++){
                System.out.print("-");
            }
            System.out.println("");
        }
    }
    public static void Opg5(){
        System.out.println("Opgave 5");
        int stairs = 8;
        for(int i=stairs;i>0;i--){
                for(int j=0;j<i+5*i-6;j++){
                    System.out.print(" ");
                }
                System.out.print(" o  ******");
                for(int j=0;j<5+6*(stairs-i)-5;j++){
                    System.out.print(" ");
                }
                System.out.println("*");
                for(int j=0;j<i+5*i-6;j++){
                    System.out.print(" ");
                }
                System.out.print("/|\\ *");
                for(int j=0;j<5+6*(stairs-i);j++){
                    System.out.print(" ");
                }
                System.out.println("*");
                for(int j=0;j<i+5*i-6;j++){
                    System.out.print(" ");
                }
                System.out.print("/ \\ *");
                    for(int j=0;j<5+6*(stairs-i);j++){
                    System.out.print(" ");
                }
                System.out.println("*");
        }
        for(int i=0;i<stairs*6+5;i++){
            System.out.print("*");
        }
        System.out.println("");
    }
}
