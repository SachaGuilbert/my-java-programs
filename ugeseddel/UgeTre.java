import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class UgeTre {
    public static void main(String[] args)
    throws IOException{
        quadratic(1,3,2);
    }

    public static void printNumbers(int makstal){
        System.out.println("Opgave 1");
        for(int i=1;i<makstal+1;i++){
            System.out.println("["+i+"]");
        }
    }
    public static void processName()
    throws IOException{
        System.out.println("Opgave 2");
        BufferedReader reader = new BufferedReader(
            new InputStreamReader(System.in)
        );
        String name = reader.readLine();
        String[] strArray = name.split(" ");
        for(int i=0;i<strArray.length;i++){
            System.out.print(strArray[strArray.length-i-1]+" ");
        }
        System.out.println();
    }
    public static void repl(String inputStr, int numberOfStr){
        System.out.println("Opgave 3");
        for(int i=0;i<numberOfStr;i++){
            System.out.print(inputStr);
        }
        System.out.println();
    }
    public static void printPowersOf2(int maxPower){
        System.out.println("Opgave 4");
        for(int i=0;i<maxPower+1;i++){
            double number = Math.pow(2, i);
            int intNumber = (int) number;
            System.out.println(intNumber);
        }
    }
    public static void analReturn(){
        System.out.println("Opgave 5");
        double balance = 0;
        double interest = 6.5 *1/100;
        int deposit = 100;
        double newbalance = 1000;
        for(int i = 0;i<25;i++){
            // for(int j=0;j<34;j++){
            //     System.out.print("_");
            // }
            // System.out.println("_");
            balance = newbalance;
            newbalance = balance*(1+interest) +deposit;
            System.out.println("|"+Math.round(balance)+"|"+interest+"|"+deposit+"|"+Math.round(newbalance)+"|");
        }
    }
    public static void quadratic(double a, double b, double c){
        System.out.println("Opgave 6");
        System.out.println((-b+Math.sqrt(b*b-4*a*c))/(2*a));
        System.out.println((-b-Math.sqrt(b*b-4*a*c))/(2*a));
    }
}
