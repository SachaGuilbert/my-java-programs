import java.util.Scanner;
import java.io.IOException;

public class OpgaveTre {
    public static void main(String[] args)
    throws IOException{
        System.out.print("Enter number of iterations: ");
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();

        double overto = 0;
        double underto = 0;

        for(int i=0;i<num;i++){
            double afstand = Math.random()*2;
            double angle = Math.random()*180;
            double ovre = afstand + Math.sin(Math.toRadians(angle))*1;
            if(ovre>2){
                overto++;
            }else{
                underto++;
            }
        }
        System.out.println(num/overto);
    }
}
