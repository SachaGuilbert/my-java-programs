public class Artikel{
    private String[] forfattere;
    private String titel;
    private Tidsskrift tidsskrift;
    private Artikel[] referenceliste;
    public Artikel(String[] forfattere, String titel, Tidsskrift tidsskrift, Artikel[] referenceliste){
        this.forfattere = forfattere;
        this.titel = titel;
        this.tidsskrift = tidsskrift;
        this.referenceliste = referenceliste;
    }
    public Artikel(String[] forfattere, String titel, Tidsskrift tidsskrift){
        this.forfattere = forfattere;
        this.titel = titel;
        this.tidsskrift = tidsskrift;
        this.referenceliste = null;
    }
    public void setReferenceliste(Artikel[] referenceliste){
        this.referenceliste = referenceliste;
    }
    public String forfString(){
        String outp = "";
        for(int i=0;i<forfattere.length;i++){
            outp = outp+ forfattere[i]+ " ";
        }
        return outp;
    }
    public String refString(){
        String outp = "";
        if(referenceliste != null){
            for(int i=0;i<referenceliste.length;i++){
                outp = outp+ referenceliste[i].titel+ ", ";
            }
        }else{
            return "No references";
        }
        outp = outp.substring(0, outp.length() - 2);
        return outp;
    }
    public String toString(){
        return "By: "+forfString()+", Article name: " + titel + ", Refences, if any: " + refString();
    }
}
