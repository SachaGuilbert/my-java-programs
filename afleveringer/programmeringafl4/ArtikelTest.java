public class ArtikelTest {
    public static void main(String[] args){
        String[] forfArr = {"Abe A.","Jordan J.","Donald D."};
        String[] forfArr2 = {"Emily M.","Cathrine C."};
        String[] forfArr3 = {"Jonathan J.","Elon M."};

        Forlag mitForlag = new Forlag("DetNyeForlag","Aarhus");
        Tidsskrift mitTids = new Tidsskrift("Den videnskabelige");
        mitTids.setForlag(mitForlag);
        Artikel minArtikel1 = new Artikel(forfArr, "Den fedeste artikel i verden", mitTids, null);
        Artikel minArtikel2 = new Artikel(forfArr2, "Den anden fedeste artikel i verden", mitTids, null);
        Artikel[] refArr = {minArtikel1,minArtikel2};
        Artikel minArtikel3 = new Artikel(forfArr3, "A study on the development of life in Bishop Rings", mitTids, refArr);
        // String[] refList = {minArtikel1};
        // Artikel minArtikel2 = new Artikel(forfArr, "Den anden fedeste artikel i verden", mitTids, refList);
        // mitTids.setISSN("1234");
        // mitTids.setForlag("1234");
        System.out.println(mitForlag);
        System.out.println(mitTids);
        System.out.println(minArtikel1);
        System.out.println(minArtikel2);
        System.out.println(minArtikel3);
    }
}
