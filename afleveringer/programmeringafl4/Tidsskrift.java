public class Tidsskrift{
    private String titel;
    private String issn;
    private Forlag forlag;
    public Tidsskrift(String titel, Forlag forlag, String issn){
        this.titel = titel;
        this.forlag = forlag;
        this.issn = issn;
    }
    public Tidsskrift(String titel){
        this.titel = titel;
        this.forlag = null;
        this.issn = "ISSN-numrene kendes ikke";
    }
    public void setISSN(String issn){
        this.issn = issn;
    }
    public void setForlag(Forlag forlag){
        this.forlag = forlag;
    }
    public String getTitel(){
        return this.titel;
    }
    public String toString(){
        return "Tidsskriftene " + titel +". Tidskriftet er fra: " + forlag.getNavn()+ ". Issn nummer: " + issn+ " ";
    }
}
