public class Opgave12 {
    public static void main(String[] args){
        primeFactors();
    }

    public static void primeFactors(){
        long n = 4294967317L;
        int factor = 2;
        while (n > 1){
            if(n % factor == 0){
                n = n/factor;
                System.out.print(factor + " ");
            }else{
                factor = factor+1;
            }
        }
    }
}
