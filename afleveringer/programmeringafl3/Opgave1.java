import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
// import java.lang.Math.sqrt;

public class Opgave1 {
    public static void main(String[] args)
    throws IOException{
        primeFactors();
    }

    public static void primeFactors()
    throws IOException{
        BufferedReader reader = new BufferedReader(
            new InputStreamReader(System.in)
        );
        boolean contProg = true;
        while(contProg = true){
            System.out.print("Enter integer greater than 1 (0 to terminate): ");
            String input = reader.readLine();
            long intInput = Integer.parseInt(input);

            if(intInput <= 0){
                System.exit(0);
            }
            //fint all prime numbers from 0 to input //only up to sqrt(intInput)
            List<Long> myList = new ArrayList<>();
            myList.add((long)2);
            boolean prime = true;
            for(long i=3;i<=intInput/2;i+=2){ //i += 2
                prime = true;
                //check for prime
                for(int j=3;j<=i/2;j+=2){ //j += 2
                    if(i % j == 0){
                        prime = false;
                    }
                }
                //if a prime is found, add to list
                if(prime == true){  
                    myList.add(i);
                }
            }
            myList.add(intInput);
            //loop through list and find factor
            for (long number : myList) {
                while(intInput % number == 0){
                    System.out.print(number + " ");
                    intInput = intInput/number;
                }
            }
            System.out.println(" ");
        }
    }
}
